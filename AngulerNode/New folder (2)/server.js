var express = require('express');
var  path = require('path');
var bodyParser = require('body-parser');

var index = require('./index');
var app=express();
var port = 3000;



//View engine
app.set ('views',path.join(__dirname,'views')) ;
app.set ('view engine','ejs') ;
app.engine('html',require('ejs').renderFile);



//Set Static Folder

app.use(express.static(path.join(__dirname,'client')));

//Body parser Mw

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: false
}));


app.use('/',index);
app.listen(port,function () {
  console.log("server run on"+port);
})
