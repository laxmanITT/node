



const mongoose = require('mongoose'); // Node Tool for MongoDB

const Schema = mongoose.Schema; // Import Schema from Mongoose

const userSchema = new Schema({
   username: { type: String, required: true,unique: false},  
   email: { type: String, required: true,unique: true },
   password: { type: String, required: true }
});







userSchema.methods.comparePassword = function(password) {
  return (password==this.password); // Return comparison of login password to password in database (true or false)
};

module.exports = mongoose.model('User', userSchema);
