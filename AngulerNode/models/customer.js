


var uniqueValidator = require('mongoose-unique-validator');
const mongoose = require('mongoose'); // Node Tool for MongoDB
mongoose.Promise = global.Promise; // Configure Mongoose Promises
const Schema = mongoose.Schema; // Import Schema from Mongoose





const customerSchema = new Schema({
    userEmail: { type: String, required: true},
	customerEmail: { type: String, required: true,unique: true },
   customerName: { type: String, required: true},  
   
   customerContact: {type: String, required: true,unique: true}
});


module.exports = mongoose.model('Customer', customerSchema);