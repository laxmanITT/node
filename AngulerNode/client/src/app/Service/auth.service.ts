import { Injectable } from '@angular/core';
import { Http, RequestOptions,Headers } from '@angular/http';
import 'rxjs/add/operator/map'
import { Observable } from "rxjs/Observable"; 
@Injectable()
export class AuthService {

    doman:string = "http://localhost:8081";
  constructor(private http:Http) { }
    
    
 registerUser(user)
    {
        
     return this.http.post(this.doman+'/authentication/register',user).map(res=>res.json()) ;  
   
    }
    
    
    
    
    getuser(user)
    {
        
     return this.http.post(this.doman+'/authentication/login',user).map(res=>res.json()) ;  
   
    }
    
	
	  saveCustomer(customer)
    {
        
     return this.http.post(this.doman+'/authentication/addCustomer',customer).map(res=>res.json()) ;  
   
    }
	
	
	  getCustomer(customer)
    {
        
     return this.http.post(this.doman+'/authentication/getcustomer',customer).map(res=>res.json()) ;  
   
    }
	
	  deletecustomer(customer)
    {
        
     return this.http.post(this.doman+'/authentication/deletecustomer',customer).map(res=>res.json()) ;  
   
    }
    
    

}
