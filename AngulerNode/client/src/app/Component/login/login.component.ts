import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {AuthService }  from '../../Service/auth.service'

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers:[AuthService]
})
export class LoginComponent {
passwordError='';
loginMessage='';

   constructor(private route: Router,private auth:AuthService)
{
	
}





    // onLoginSubmit method perform the
    onLoginSubmit(loginUser:any)
    {

        var self = this;
        
        this.auth.getuser(loginUser).subscribe(function(data){
              

            if(data.success)
            {
              
           
				self.route.navigate(['/dashboard',loginUser.email]);
        
            }
            else
            {
                if(data.message=="Enter correct password")
                {  
                     self.passwordError = data.message; 
                   
                }

                else{

                    self.loginMessage = data.message;
               
                }

            }


        });





    }



}
