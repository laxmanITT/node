import { Component} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {AuthService }  from '../../Service/auth.service'
import {CustomerListService }  from '../../Service/CustomerService'

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css'],
	providers:[AuthService,CustomerListService]
})
export class DashboardComponent {

	errorMessage:string=''
	userName: string = '';
	email: string = '';
	public customerList: any[];
	customerName: string = "";
	customerEmail: string = "";
	customerContact: string = "";

	public List: any[] = [{

		customerName: 'Customer Name',
		customerEmail: 'Customer Email',
		customerContact: 'Customer Contact'

	}];



	constructor(private route: ActivatedRoute,private auth:AuthService, private customerservice: CustomerListService) {

		this.email = route.snapshot.params['email']; 
		this.getCustomer( this.email);

	}



	onSubmit(customer: any) {
		this.errorMessage="";
		customer.userEmail = this.email;
		var pointer =this;
		this.customerservice.SetCustmerList(customer);
		this.auth.saveCustomer(customer).subscribe(function(data){

			

			if(data.success)
			{  

				
				pointer.getCustomer(pointer.email)		
				pointer.customerName = "";
				pointer.customerEmail = "";
				pointer.customerContact = "";
			}
			else
			{
			
				pointer.errorMessage = data.message;
			}


		});



	}



	getCustomer(email)
	{
		var customer={'userEmail':email};

		console.log(customer);
		var pointer =this;

		this.auth.getCustomer(customer).subscribe(function(data){
			console.log(data);
			console.log(customer);
			if(data.success)
			{  
				console.log(data.message);	 

				pointer.customerList = data.message;
			}
			else
			{
				console.log(data.message);
				pointer.customerList=[];
			}


		});
	}
	
	
onDelete(customer: any) {
    		var pointer =this;
 	     this.auth.deletecustomer(customer).subscribe(function(data){
			console.log(data);
			console.log(customer);
			if(data.success)
			{  
				pointer.getCustomer(pointer.email)	
			}
		});

  }



}
