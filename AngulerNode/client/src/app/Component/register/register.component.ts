import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {AuthService }  from '../../Service/auth.service'

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css'],
    providers:[AuthService]
})
export class RegisterComponent  {
        errorMessage='';

constructor(private route: Router,private auth:AuthService)
{
	
}

onSubmit(user:any)
{

    var self = this;
       this.auth.registerUser(user).subscribe(function(data){
        if(data.success)
        {  
			self.errorMessage = data.message; 
			self.route.navigate(['/dashboard',user.email]);
           
        }
        else
        {
            self.errorMessage = data.message;   
        }


    });

}










}
