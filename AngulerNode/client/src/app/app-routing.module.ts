import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core';
import { DashboardComponent } from './Component/dashboard/dashboard.component';
import { RegisterComponent } from './Component/register/register.component';
import { LoginComponent } from './Component/login/login.component';
import { IndexComponent } from './Component/index/index.component';

// Our Array of Angular 2 Routes
const appRoutes: Routes = [
	{ path: '' , redirectTo :'/index',pathMatch: 'full'},
	{ path: 'index' ,component:IndexComponent ,
	 children: [
		 { path: '' , component: LoginComponent, outlet: 'child1'},
		 {path: 'Login' , component: LoginComponent, outlet: 'child1' },
		 {path: 'Registration' , component: RegisterComponent, outlet: 'child1'}
	 ]},
	
	{path: 'dashboard/:email' , component:  DashboardComponent},
	{path: '**' , component:  IndexComponent}

];
@NgModule({
	declarations: [],
	imports: [RouterModule.forRoot(appRoutes, { enableTracing: true })],
	providers: [],
	bootstrap: [],
	exports: [RouterModule]
})

export class AppRoutingModule { }
