const express = require('express'); // Fast, unopinionated, minimalist web framework for node.
const app = express(); // Initiate Express Application
var cors = require('cors')
const router = express.Router(); // Creates a new router object.
const mongoose = require('mongoose'); // Node Tool for MongoDB
const config = require('./config/database'); // Mongoose Config
const path = require('path'); // NodeJS Package for file paths
mongoose.Promise = global.Promise; // Configure Mongoose Promises
const bodyParser = require('body-parser'); 

// Middleware
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
	extended: false
}));

const authentication = require('./routes/authentication')(router); // Import Authentication Routes




app.use(cors({orign:'http://localhost:4200'}));

// Database Connection

mongoose.connect(config.uri, function(err){
	if (err) {
		console.log('Could NOT connect to database: ', err);
	} else {
		console.log('Connected to database: ' );
	
	}
});




app.use(express.static(__dirname + '/client/dist/')); // Provide static directory for frontend
app.use('/authentication', authentication);
app.get('/home', function(req, res){

	
	res.sendFile(path.join(__dirname + '/client/dist/index.html'));
});



app.listen(8081, function(){
	console.log('Listening on port 8081');
});