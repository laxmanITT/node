const User = require('../models/user');
const Customer = require('../models/customer');// Import User Model Schema

module.exports = function(router){
	/* ==============
     Register Route
  ============== */
	router.post('/register', function(req, res){
		// Check if email was provided


		let user = new User({
			username: req.body.username.toLowerCase(),
			email: req.body.email.toLowerCase(),
			password: req.body.password
		});


		// Save user to database
		user.save(function(err){
			// Check if error occured
			if (err) {
				// Check if error is an error indicating duplicate account
				if (err.code === 11000) {
					res.json({ success: false, message: 'e-mail already exists' }); // Return error
				} else {
					// Check if error is a validation rror
					if (err.errors) {
						// Check if validation error is in the email field
						if (err.errors.email) {
							res.json({ success: false, message: err.errors.email.message }); // Return error
						} else {
							// Check if validation error is in the username field
							if (err.errors.username) {
								res.json({ success: false, message: err.errors.username.message }); // Return error
							} else {
								// Check if validation error is in the password field
								if (err.errors.password) {
									res.json({ success: false, message: err.errors.password.message }); // Return error
								} else {
									res.json({ success: false, message: err }); // Return any other error not already covered
								}
							}
						}
					} else {
						res.json({ success: false, message: 'Could not save user. Error: ', err }); // Return error if not related to validation
					}
				}
			} else {
				res.json({ success: true, message: 'Acount registered!' }); // Return success
			}
		});



	});


	//     Login Route
	router.post('/login', function(req, res){
		// Check if email was provided


		User.findOne({email: req.body.email.toLowerCase()},function(err,user){
			if(err)
			{

				res.json({ success: false, message: err });
			}
			else
			{
			
				if(!user)
				{
					res.json({ success: false, message: 'your email is not register'});
				}
				else{

					const validpassword = user.comparePassword(req.body.password);
					if(!validpassword)
					{
						res.json({ success: false, message: 'Enter correct password' });
					}
					else
					{
						res.json({ success: true, message: 'Login Sucess' });
					}

				}
			}
		});


		// Save user to database

	});









	//     Add customer Route
	router.post('/addCustomer', function(req, res){
		// Check if email was provided
		let customer = new Customer({
			userEmail: req.body.userEmail,
			customerName: req.body.customerName,
			customerEmail: req.body.customerEmail,
			customerContact: req.body.customerContact
		});


		customer.save(function(err){
			// Check if error occured
			if (err) {
				// Check if error is an error indicating duplicate account
				if (err.code === 11000) {
					res.json({ success: false, message: "Customer email Already exist"}); // Return error
				} else {


					res.json({ success: false, message: 'Could not save user. Error: ', err }); // Return error if not related to validation

				}
			} else {
				res.json({ success: true, message: 'Customer registered!' }); // Return success
			}
		});


		// Save user to database

	});



	//get customer
	router.post('/getcustomer', function(req, res){
		// Check if email was provided


		Customer.find({userEmail: req.body.userEmail},function(err,customer){
			if(err)
			{

				res.json({ success: false, message: err });
			}
			else
			{
				if(!customer)
				{
					res.json({ success: false, message: 'your email is not registers'});
				}
				else{

					res.json({ success: true, message:customer});

				}
			}
		});


		// Save user to database

	});




	//delete customer
	router.post('/deletecustomer', function(req, res){
		// Check if email was provided


		Customer.findOne({customerEmail: req.body.customerEmail},function(err,customer){
			if(err)
			{

				res.json({ success: false, message: err });
			}
			else
			{
				if(!customer)
				{
					res.json({ success: false, message: 'your email is not registers'});
				}
				else{

					customer.remove(function (err) {

						if(err)
						{
							res.json({ success: false, message: 'error'});
						}
						else
						{
							res.json({ success: true, message:"sucess"});
						}


					});



				}
			}
		});


		// Save user to database

	});

	return router; // Return router object to main index.js
}
