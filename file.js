var http = require('http');
var fs = require('fs');
var url = require('url');
var formidable = require('formidable');

http.createServer(function(req, res) {
  if (req.url == '/fileupload') {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
      var oldpath = files.filetoupload.path;
      var newpath = 'E:\TRAINING\Node\Image/' + files.filetoupload.name;

      fs.rename(oldpath, newpath, function(err) {
        if (err) throw err;
        res.write('File uploaded and moved!');
        res.end();
      });
    });
  } else {

    fs.readFile('file.html', function(err, data) {
      res.writeHead(200, {
        'Content-Type': 'text/html'
      });
      res.end(data);
    })
  }
}).listen(8081);
