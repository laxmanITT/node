var http = require('http');
var currentdate = require('./date');
var fs = require('fs');
var url = require('url');

var serverFunction = function(req, res) {
    var urlObject = url.parse(req.url, true);
    var filename = "." + urlObject.pathname;

    fs.readFile(filename, function(err, data) {
        res.writeHead(200, {
            'Content-Type': 'text/html'
        });
      res.end(data);
        console.log(filename);
    })

}


http.createServer(serverFunction).listen(8081);
